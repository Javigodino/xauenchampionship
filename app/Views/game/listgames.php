<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <!-- <link rel="stylesheet" href="cl-icon/css/all.min.css"> -->

</head>

<body>
<h1 class="title" >Aqui puedes obtener información sobre los diferentes juegos en los que puedes participar</h1>
  <div class="card text-center" style="margin-bottom: 3%;" >
    <div class="card-header">
      <ul class="nav nav-tabs card-header-tabs" id="myTabs">
        <?php foreach ($games as $row): ?>
          <li class="nav-item">
            <a class="nav-link" href="#" onclick="changeContent('<?php echo $row->Name; ?>')"><?php echo $row->Name; ?></a>
          </li>
        <?php endforeach; ?>
      </ul>
      </div>
      <div class="card-body" id="contentContainer">
        <div id="<?php echo $row->Name; ?>" >
          <img src="<?php echo $row->Logo; ?>" alt="" style="height:40%; width: 60%">
          <h5 class="card-title" style="margin-top: 10px;"><?php echo $row->Name; ?></h5>
          <p class="card-text"" ><?php echo $row->Description; ?></p>
          <a href="<?php echo $row->url_download; ?>" class="btn btn-secondary" target="_blank">Obtener</a>
      </div>
    </div>

    

    <script>
      function changeContent(game) {
          var contentContainer = document.getElementById('contentContainer');
          var img = contentContainer.querySelector('img');
          var navLinks = document.querySelectorAll('.nav-link');
          navLinks.forEach(function (link) {
              link.classList.remove('active');
          });
          event.target.classList.add('active');
          var a = contentContainer.querySelector('a');

          switch (game) {
              <?php foreach ($games as $row): ?>
              case '<?php echo $row->Name; ?>':
                  img.src = '<?php echo $row->Logo; ?>';
                  img.style = 'height:40%; width: 60%';
                  contentContainer.querySelector('h5').innerText = '<?php echo $row->Name; ?>';
                  contentContainer.querySelector('p').innerText = '<?php echo $row->Description; ?>';
                  a.href="<?php echo $row->url_download; ?>";
                  break;
              <?php endforeach; ?>
              default:
                  break;
          }
      }
    </script>
  </div>  
</body>
