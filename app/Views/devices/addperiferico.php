<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio de sesión</title>
    <link rel="stylesheet" href="/css/style.css">
    <!-- <link rel="stylesheet" href="cl-icon/css/all.min.css"> -->

</head>
<body>
<div style="margin-bottom: 80px;">
</div>
    <div align="center" >
        <div class="cardperiferico" >
            <form action="/devices/addperiferico" class="form" method="post">
                <div class="select-editado"style="margin-top:2%">
                    <label for="name">Selecciona el tipo de periférico:</label>   
                    <select name="select" id="valor3">                        
                        <option value="cascos">cascos</option>
                        <option value="monitor">monitor</option>
                        <option value="raton">raton</option>
                        <option value="teclado">teclado</option>                        
                    </select>
                </div>
                <div class="inp">
                    <label for="mail">Descripción:</label>
                    <input  class="input" type="text" id="valor2" name="valor2">
                </div>
                <button class="submit">Añadir Periférico</button>
            </form>
        </div>
        <div>
            <?php if (session()->has('msg')): ?>
                <div class="alert alert-danger" style="max-width: 30vw;">
                    <?= session('msg') ?>
                </div>
            <?php endif ?>
        </div>
    </div>
    

</body>