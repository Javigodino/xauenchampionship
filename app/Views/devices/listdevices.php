<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio de sesión</title>
    <link rel="stylesheet" href="/css/style.css">
    <!-- <link rel="stylesheet" href="cl-icon/css/all.min.css"> -->

</head>

<h1 class="title" >Si necesitas un periférico para jugar, esta es tu página</h1>



<div style="display: flex; justify-content: space-between; margin: 0 auto 20px; max-width: 1000px;">
    <?php 
    $counter = 0;
    foreach ($devices as $row):
        if ($counter % 3 == 0 && $counter != 0) { 
            echo '</div><div style="display: flex; justify-content: space-between; margin: 0 auto 20px; max-width: 1000px;">';
        }
    ?>
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="<?php echo $row->Logo; ?>">
            <center>
            <div class="card-body">
                <h3><?php echo $row->Name; ?></h3>
                <button type="button" class="btn btn-outline-secondary" data-bs-toggle="popover" data-bs-placement="bottom"  data-bs-content="<?php echo $row->Description; ?>">
                    Ver descripción
                </button>
                <?php 
                $session = session(); 
                if ($session->get('logged_in')):
                    $rented=new \App\Models\DeviceRentedModel();
                    $info=$rented->find($row->id);
                        if(!$info):
                ?>
                    <?php if ($session->get('user')->rol == 2):?>
                    <form action="/devices/deleteDevice/<?php echo $row->id; ?>" method="delete">
                        <button type="submit" class="btn btn-danger" style="margin-top: 4px; margin-bottom: 4px;" >Eliminar Dispositivo</button>
                    </form>
                    <?php endif; ?>
                    
                    <form action="/devices/updateDevice/<?php echo $row->id; ?>" method="update">
                        <button type="submit" class="btn btn-primary" style="margin-top: 4px;">Reservar</button>
                    </form>
                    <?php else: 
                        if($info->username==$session->get('user')->name):
                    ?>
                            <form action="/devices/returnDevice/<?php echo $row->id; ?>" method="delete">
                                <button type="submit" class="btn btn-success" style="margin-top: 4px;">Devolver</button>
                            </form>
                        <?php else: ?>
                            <button type="button" class="btn btn-danger" data-bs-toggle="popover" data-bs-placement="bottom"  data-bs-content="Articulo reservado">Reservado</button>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php else: ?>
                    <a href="/user/login">
                        <button type="button" class="btn btn-primary" data-bs-toggle="popover" data-bs-placement="bottom"  data-bs-content="Debes iniciar sesión">Reservar</button>
                    </a>
                <?php endif; ?>
            </div>
            </center>
        </div>
    <?php 
    $counter++;
    endforeach; 
    ?>
</div>


<script>
    var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
    var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
        var options = {
            trigger: 'hover' 
        };
        return new bootstrap.Popover(popoverTriggerEl, options);
    })
</script>

<div style="margin-bottom: 30px">
    <center>
        <?php $session = session(); if ($session->get('logged_in')): ?>
            <a href="/devices/addperiferico" justify-content="center">
                <button class="submit" type="submit">Añadir Periférico</button>
            </a>
        <?php endif; ?>     
    </center>
</div>
