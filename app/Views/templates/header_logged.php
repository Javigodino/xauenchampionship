<!doctype html>
<html class="h-100">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title style="text-color: #fff">XauenChampionship</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>

    <style>
        .container .navbar-brand {
            font-size: 3vw;
            text-align: center;
            color: #fff;
            position: absolute;
            top: 10px;
            left: 50%;
            transform: translateX(-50%);
        }
        .container .btn-menu label{
            color: #fff;
            font-size: 2vw;
            cursor: pointer;
            position: absolute;
            top: 10px;
            left: 10px;
        }
        .container .menu {
            position: absolute;
            top: 10px;
            right: 10px;
        }
        #btn-menu{
            display: none;
        }
        .container-menu{
            position: absolute;
            background: rgba(0,0,0,0.5);
            width: 100%;
            height: 100vh;
            top: 0;left: 0;
            transition: all 500ms ease;
            opacity: 0;
            visibility: hidden;
            z-index: 1;
        }
        #btn-menu:checked ~ .container-menu{
            opacity: 1;
            visibility: visible;
        }
        .cont-menu{
            width: 100%;
            max-width: 16vw;
            background: #1c1c1c;
            font-size: 1.4vw;
            height: 100vh;
            position: relative;
            transition: all 500ms ease;
            transform: translateX(-100%);
        }
        #btn-menu:checked ~ .container-menu .cont-menu{
            transform: translateX(0%);
        }
        .cont-menu nav{
            transform: translateY(15%);
        }
        .cont-menu nav a{
            display: block;
            text-decoration: none;
            padding: 20px;
            color: #c7c7c7;
            border-left: 5px solid transparent;
            transition: all 400ms ease;
        }
        .cont-menu nav a:hover{
            border-left: 5px solid #c7c7c7;
            background: #1f1f1f;
        }
        .cont-menu label{
            position: absolute;
            right: 5px;
            top: 10px;
            color: #fff;
            cursor: pointer;
            font-size: 18px;
        }
    </style>

</head>

<body class="d-flex flex-column h-100">

<header class="header">
		<div class="container">
            <div class="btn-menu">
                <label for="btn-menu">☰</label>
                <a class="navbar-brand" href="/">XauenChampionship</a>
            </div>
			<nav class="menu">
                <a href="/user/logout">
                        <button class="btn btn-outline-danger" type="submit">Cerrar Sesión</button>
                </a>
			</nav>
		</div>
</header>
<!-- Begin page content -->
<main class="flex-shrink-0">
    <div class="container">
