<input type="checkbox" id="btn-menu">
<div class="container-menu">
	<div class="cont-menu">
		<nav>
            <a class="nav-link" href="/game/listgames">Juegos</a>
			<a class="nav-link" href="/tournaments/listtournaments">Torneos</a>
			<a class="nav-link" href="/devices/listdevices">Alquiler de periféricos</a>
			<a class="nav-link" href="/user/personal">Sobre Nosotros</a>
			<?php 
                $session = session(); 
                if ($session->get('logged_in')):
            ?>
            <a class="nav-link" href="/user/datos">Datos Usuario</a>
            <?php endif?>
		</nav>
		<label for="btn-menu">✖️</label>
	</div>
</div>
</body>