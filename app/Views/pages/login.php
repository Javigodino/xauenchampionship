<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio de sesión</title>
    <link rel="stylesheet" href="/css/EstiloLogin.css">
    <!-- link rel="stylesheet" href="cl-icon/css/all.min.css"> -->

</head>
    <body>
        <div align="center">
            <div class="wrapper" style="margin-top: 5vw">
                <form action="<?php echo base_url('user/login') ?>" method="post" class="form">
                    <h1 class="title">Inicio de sesión</h1>
                    <div class="inp">
                        <label for="user"></label>
                        <input type="text" name="user" id="user" class="input" placeholder="Usuario">
                        <i class="fa-solid fa-user"></i>
                    </div>
                    <div class="inp">
                        <label for="password"></label>
                        <input type="password" name="password" id="password" class="input" placeholder="Contraseña">
                        <i class="fa-solid fa-lock"></i>
                    </div>
                    <button class="submit">Iniciar Sesión </button>
                    <p class="footer"> ¿No tienes cuenta?<a class="link" href="/user/registro">Realizar registro</a></p>
                </form>
                <div class="banner">
                    <img src="/img/xauen.png" width="150vw" height="150vh">
                </div>
                </form>

            </div>
            <div style="margin-bottom: 80px;margin-top: 20px">
            <?php if (session()->has('msg')): ?>
                    <div class="alert alert-danger" style="max-width: 20vw;">
                        <?= session('msg') ?>
                    </div>
                <?php endif ?>
            </div>
        </div>

    </body>

