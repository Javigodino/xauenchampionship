<div style="margin-top: 20px; margin-bottom: 40px;">
    <h2>Si necesitas un periferico para jugar esta es tu pagina</h2>
</div>

<div style="display: flex; justify-content: space-between; margin: 0 auto 20px; max-width: 1000px;">
    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="/img/monitor.jpg" alt="Card image cap">
        <div class="card-body">
            <h3>Periferico 1</h3>
            <p class="card-text">Descripción del periferico 1.</p>
        </div>
    </div>

    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="/img/raton.jpg" alt="Card image cap">
        <div class="card-body">
            <h3>Periferico 2</h3>
            <p class="card-text">Descripción del periferico 2.</p>
        </div>
    </div>

    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="/img/teclado.jpg" alt="Card image cap" >
        <div class="card-body">
            <h3>Periferico 3</h3>
            <p class="card-text">Descripción del periferico 3.</p>
        </div>
    </div>
</div>


<a href="/pages/addperiferico">
    <button class="btn btn-outline-secondary" type="submit">Añadir Periferico</button>
</a>