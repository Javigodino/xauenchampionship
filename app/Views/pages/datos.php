<!DOCTYPE html>
<html lang="es">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informaci�n del usuario</title>
    <link rel="stylesheet" href="/css/EstiloLogin.css">
    <!-- link rel="stylesheet" href="cl-icon/css/all.min.css"> -->

</head>
 <body>
        <div style="margin-bottom: 80px;margin-top: 20px"> </div>
        <div align="left">
            <h1 class="title">Información del usuario: <?php $session = session(); echo $session->get('user')->name ?></h1>
                <h2>Listado de Juegos</h2>
                <div>
                <table>
                    <tr>
                        <th style="text-align: center;">Nombre</th>
                        <th style="text-align: center;">Logo</th>
                        <th style="text-align: center;">Boton</th>
                    </tr>
                    <?php foreach ($games as $juego): ?>
                        <tr> 
                            <td style="text-align: center;"> <?php echo $juego->Name; ?> </td>
                            <td style="text-align: center;"> <img src="<?php echo $juego->Logo; ?>" style="height:5%; width: 10%"> </td>
                            <?php 
                                $session = session(); 
                                $GamesRentedComprobar = new \App\Models\GamesRentedModel();
                                $encontrado = $GamesRentedComprobar->where('user', $session->get('user')->name)
                                                ->where('game', $juego->Name)
                                                ->first();
                            ?>
                            <?php if ($encontrado): ?>
                                <form action="/user/borrar/<?php echo $juego->Name; ?>" method="delete">
                                    <td style="text-align: center;"> <button class="submit" > - </button> </td>
                                </form>
                            <?php else: ?>
                                <form action="/user/AgregarJuego/<?php echo $juego->Name; ?>" method="post">
                                    <td style="text-align: center;">  <button class="submit" > + </button></td>
                                </form>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                </table>


                </div>
              
        </div>
</body>
</html>