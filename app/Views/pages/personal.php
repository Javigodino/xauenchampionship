<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/EstiloLogin.css">

    <title>Información del personal</title>
   
</head>
<body>

    <h1 style="margin-top: 6%;">Información del personal</h1>


    <div style="margin-bottom: 2%;"></div>

    <div class="contenedor" align="center">

    <div class="wrapper2">
        <h2>cpc0004</h2>
        <div style="margin-bottom: 5%;"></div>
        <img src="/img/carlos.jpg" width="40%" height="35%">
        <div style="margin-bottom: 5%;"></div>
        <p><strong>Nombre:</strong> Carlos Pérez Cañada</p>
        <p><strong>Correo:</strong> cpc0004@red.ujaen.es</p>
        <p><strong>Descripción:</strong> Ingeniero informático dedicado y trabajador, apasionado por la tecnología y siempre dispuesto a enfrentar nuevos desafíos en el mundo de la informática.</p>
    </div>

    <div class="wrapper2" >
        <h2>fgc00052</h2>
        <div style="margin-bottom: 5%;"></div>
        <img src="/img/fran.jpg" width="40%" height="35%">
        <div style="margin-bottom: 5%;"></div>
        <p><strong>Nombre:</strong> Francisco García del Castillo</p>
        <p><strong>Correo:</strong> fgc00052@red.ujaen.es</p>
        <p><strong>Descripción:</strong>  Ingeniero informático comprometido y diligente, con una pasión por la resolución de problemas y el desarrollo de soluciones innovadoras en el ámbito de la tecnología.</p>
    </div>

    <div class="wrapper2">
        <h2>jgm01126</h2>
        <div style="margin-bottom: 5%;"></div>
        <img src="/img/javi.jpg" width="40%" height="35%">
        <div style="margin-bottom: 5%;"></div>
        <p><strong>Nombre:</strong> Javier Godino Moral</p>
        <p><strong>Correo:</strong> jgm00116@red.ujaen.es</p>
        <p><strong>Descripción:</strong> Ingeniero informático tenaz y enfocado, con una ética laboral excepcional y una determinación inquebrantable para alcanzar metas profesionales y superar obstáculos en su camino hacia el éxito. </p>
    </div>
    </div>
</body>
</html>
