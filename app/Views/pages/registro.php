<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar cuenta</title>
    <link rel="stylesheet" href="/css/EstiloLogin.css">
    <!-- <link rel="stylesheet" href="cl-icon/css/all.min.css"> -->
</head>
    <body> 
    <div style="margin-bottom: 80px;"></div>
    <?php if (session()->has('msg')): ?>
                <div class="alert alert-danger">
                    <?= session('msg') ?>
                </div>
            <?php endif ?>
        </div>
    <div align="center">
        <div class="wrapper">
            <form action="<?php echo base_url('user/registro') ?>" method="post" class="form">
                <h1 class="title">Registrar cuenta</h1>
                <div class="inp">
                    <label for="nombre" label>
                    <input type="text" name="nombre" id="nombre" class="input" placeholder="Nombre">
                    <i class="fa-solid fa-user"></i>
                </div>
                <div class="inp">
                    <label for="password"></label>
                    <input type="password" name="password" id="password" class="input" placeholder="Contraseña">
                    <i class="fa-solid fa-lock"></i>
                </div>
                <div class="inp">
                    <label for="password2"></label>
                    <input type="password" name="password2" id="password2" class="input" placeholder="Confirmar contraseña">
                    <i class="fa-solid fa-lock"></i>
                </div>
                <button class="submit">Registrarse</button>

                </form>
                    <div class="banner">
                        <img src="/img/xauen.png" width="150vw" height="150vh">
                    </div>
                </div>
         </div>


</body>
