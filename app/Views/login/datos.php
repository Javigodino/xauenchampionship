
 <body>
        <div align="center">
            <div class="wrapper">
                <form action="<?php echo base_url('user/login') ?>" method="post" class="form">
                    <h1 class="title">Inicio de sesi�n</h1>
                    <div class="inp">
                        <label for="user"></label>
                        <input type="text" name="user" id="user" class="input" placeholder="Usuario">
                        <i class="fa-solid fa-user"></i>
                    </div>
                    <div class="inp">
                        <label for="password"></label>
                        <input type="password" name="password" id="password" class="input" placeholder="Contrase�a">
                        <i class="fa-solid fa-lock"></i>
                    </div>
                    <button class="submit">Iniciar Sesi�n </button>
                    <p class="footer"> �No tienes cuenta?<a class="link" href="/user/registro">Realizar registro</a></p>
                </form>
                <div class="banner">
                    <img src="/img/xauen.png" width="150vw" height="150vh">
                </div>
                </form>

            </div>
        </div>

    </body>
