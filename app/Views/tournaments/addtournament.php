<?php $session = session();
if ($session->get('logged_in') && $session->get('user')->rol == 2): ?>
    <link rel="stylesheet" href="/css/EstiloTorneo.css">
    <h1 class="title">Inserte los valores para la creación de un torneo</h1>

    <div class="box2">
        <form action='/tournaments/addtournament' method="post" style="margin-top: 20px;" enctype="multipart/form-data">
            <ul style="list-style-type: none; padding: 0;">
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="Image">Inserte imagen:</label>
                    <input type="file" id="image" name="image">
                </li>
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="game">Seleccione un juego:</label>
                    <select name="game_id">
                        <?php foreach ($games as $row): ?>

                            <option value='<?php echo $row->Name; ?>'><?php echo $row->Name; ?></option>

                        <?php endforeach; ?>

                    </select>
                    <input type="hidden" id="image_id" name="image_id">

                </li>
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="name">Nombre del torneo:</label>
                    <input type="text" id="nombre" name="nombre">
                </li>
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="mail">Descripción:</label>
                    <input type="text" id="descripcion" name="descripcion">
                </li>
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="date">Dia:</label>
                    <input type="date" id="fecha" name="fecha" value="<?php echo date("Y-m-d"); ?>">
                </li>

                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="hour">Hora:</label>
                    <input type="time" id="hora" name="hora">
                </li>
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="maxPlayers">Maximo de jugadores permitidos:</label>
                    <input type="number" id="maxJug" name="maxJug">
                </li>
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="inscriptionCost">Coste para inscribirse:</label>
                    <input type="number" id="costeInscrip" name="costeInscrip">
                </li>
                <li style="margin-bottom: 5px;">
                    <label class="descripcion-label" for="reward">Premio(escrito con letra solo):</label>
                    <input type="text" id="premio" name="premio">
                </li>
            </ul>
            <div style="text-align: center;">
                <button class="submit"> Crear torneo </button>
            </div>
        </form>
    </div>
<?php else: ?>
    <link rel="stylesheet" href="/css/style.css">
    <h1 class="title" style="max-height: 3vw;">No tienes permisos para acceder a esta sección.</h1>
<?php endif; ?>