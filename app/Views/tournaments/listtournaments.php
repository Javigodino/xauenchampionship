<link rel="stylesheet" href="/css/EstiloTorneo.css">

<h1 class="title" style="margin-top: 7%;">Listado de torneos</h1>

<body>
    <div style="text-align: center;">
    <?php foreach ($tournamentsList as $row): ?>
      <a class="nav-link" href="/tournaments/tournament_details/<?php echo $row->Id_Tournament; ?>">
        <div class="box">
          <div class="box3"><img src="<?php echo env('uploadPrefix') . $row->Logo; ?>" alt="Imagen 1" ></div>
          <p><?php echo $row->Description; ?></p>
        </div>
      </a>
    <?php endforeach; ?>
  </div>

  <div class="nav-item" style="text-align: center;">
    <a class="nav-link" href="/tournaments/addtournament">
    <?php $session = session(); if ($session->get('logged_in') && $session->get('user')->rol == 2): ?> 
      <div><button class="submit"> + </button></div>
    <?php endif; ?>
    </a>
  </div>

</body>