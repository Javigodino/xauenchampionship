<link rel="stylesheet" href="/css/EstiloTorneo.css">
<h1 class="title">Detalles del Torneo</h1>
<div class="cajaInfo">
    <h2 ><?php echo $tournamentInfo->Name; ?></h2>

    <img src="<?php echo env('uploadPrefix') . $tournamentInfo->Logo; ?>" alt="Imagen 1" style="height:100%; width: 40%">

    <p><strong>Juego referente:</strong> <?php echo $tournamentInfo->Game; ?></p>
    <p><?php echo $tournamentInfo->Description; ?></p>
    <p><strong>Fecha:</strong> <?php echo $tournamentInfo->Tournament_Date; ?></p>
    <p><strong>Hora:</strong> <?php echo $tournamentInfo->Tournament_Hour; ?></p>
    <p><strong>Inscripción:</strong> <?php echo $tournamentInfo->Inscription_Cost; ?> €</p>
    <p><strong>Premios:</strong> <?php echo $tournamentInfo->Reward; ?></p>
    <table>
    <tr>
        <th>Usuarios Inscritos</th>
    </tr>
    <?php foreach ($userRegistered as $row1): ?>
        <tr>
            <td><?php echo $row1->User_Name; ?></td>
        </tr>
    <?php endforeach; ?>

</table>
    <div>
        <?php $session = session(); if ($session->get('logged_in') ): ?>
            <?php $session = session(); if ($session->get('logged_in') && $session->get('user')->rol == 2):?>
                <form action="/tournaments/deleteTournament/<?php echo $tournamentInfo->Id_Tournament; ?>" method="delete">
                    <button type="submit" class="btn btn-danger">Borrar Torneo</button>
                </form>
            <?php else: ?>
                <?php $session = session(); $userTournamentModelComprobar = new \App\Models\UserTournamentModel();
                $encontrado=$userTournamentModelComprobar->where('User_Name',$session->get('user')->name)
                ->where('Id_Tournament',$tournamentInfo->Id_Tournament)
                ->first();
                 if ($encontrado):?>
                    <form action="/tournaments/deregister/<?php echo $tournamentInfo->Id_Tournament; ?>/<?php  echo $session->get('user')->name; ?>" method="delete">
                    <button class="submit"> Desapuntarse </button>
                <?php else: ?>
                    <form action="/tournaments/registerTournament/<?php echo $tournamentInfo->Id_Tournament; ?>/<?php  echo $session->get('user')->name; ?>" method="post">
                    <button class="submit"> Apuntarse </button>
                <?php endif; ?>   
            <?php endif; ?>
        <?php else: ?>
            <a href="/user/login">
            <button class="submit"> Registrarse </button>
        </a>
        <?php endif; ?>
    </div>
</div>
