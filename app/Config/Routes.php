<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Pages::getView');
$routes->get('pages/(:any)', 'Pages::getView/$1');

$routes->setAutoRoute(true);

