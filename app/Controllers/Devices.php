<?php
namespace App\Controllers;
use CodeIgniter\Controller;
class Devices extends BaseController{
    public function getListdevices(){

        $devicesModel = new \App\Models\DevicesModel();
        $session = session();
        $devicerentedModel = new \App\Models\DeviceRentedModel();
        $data['devices_rented'] = $devicerentedModel->findAll();
        $data['devices'] = $devicesModel->findAll();
        $data['content'] = view('devices/listdevices', $data);
        if(!session('logged_in')){
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
        }
        else{
             echo view("templates/header_logged", $data); 
             echo view("templates/lateral", $data);
        }
        echo view("devices/listdevices", $data);
        echo view("templates/footer", $data);
    }
    public function getAddPeriferico($page = "addperiferico")
    {
        if (! is_file(APPPATH.'Views/devices/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);
        $devicesModel = new \App\Models\DevicesModel();
        $data['devices'] = $devicesModel->findAll();        
        if(!session('logged_in')){
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
            return redirect()->to('user/login');
        }
        else{
             echo view("templates/header_logged", $data); 
             echo view("templates/lateral", $data);
        }
        echo view("devices/".$page, $data);
        echo view("templates/footer", $data);
    }

    public function postAddPeriferico()
    { 
        $request = \Config\Services::request();
        $validation = \Config\Services::validation();
        
        $rules = [
            'valor2' => [
                'label' => 'valor2',
                'rules' => 'required|alpha_space'
            ],
            'select' => [
                'label' => 'select',
                'rules' => 'required'
            ]
            
        ];
        $data = [];
        $session = session();

        if ($this->validate($rules)) {
            
            $nameD = $request->getPost('select');
            $descriptionD = $request->getPost('valor2');
            $data = [
                'Name' => $nameD,
                'Description'    => $descriptionD,
                'Logo'    => "/img/$nameD.jpg",
                'Rented'    => '0'
            ];
            $devicesModel = new \App\Models\DevicesModel();
            if($devicesModel->insert($data)){
                return redirect()->to(base_url('devices/listdevices'));
            }else{
                $session->setFlashdata('msg', 'Formato de descripcion no aceptado');
                return redirect()->to(base_url('devices/addperiferico'));
            }
        } else {            
            $data['errors'] = $validation->getErrors();
            $session->setFlashdata('msg', 'No has insertado una descripción o descripcion no válida');
            return redirect()->to(base_url('devices/addperiferico'));
        }
        
    }
    public function getupdateDevice($id)
    {
        $request = \Config\Services::request();

        $deviceModel = new \App\Models\DevicesModel();
        $session = session();
        
        if(!$deviceModel->find($id)){

            return redirect()->to(base_url('devices/listdevices'));;

        }else{
            $data = [
                'id_device' => $id,
                'username' => $session->get('user')->name,
            ];
            $devicerentedModel = new \App\Models\DeviceRentedModel();
            $devicerentedModel->insert($data);

            return redirect()->to(base_url('devices/listdevices'));
        }
    }

    public function getdeleteDevice($id)
    {
        $request = \Config\Services::request();

        $devicesModel = new \App\Models\DevicesModel();
        $devicerentedModel = new \App\Models\DeviceRentedModel();
        $session = session();
        
        if($devicesModel->find($id)){
            $devicesModel->delete($id);
        }

        if($devicerentedModel->find($id)){
            $devicerentedModel->delete($id);
        }

        return redirect()->to(base_url('devices/listdevices'));
    }

    public function getreturnDevice($id)
    {
        $request = \Config\Services::request();

        $devicerentedModel = new \App\Models\DeviceRentedModel();
        $session = session();

        if($devicerentedModel->find($id)){
            $devicerentedModel->delete($id);
        }

        return redirect()->to(base_url('devices/listdevices'));
    }
}