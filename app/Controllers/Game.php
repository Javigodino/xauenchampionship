<?php
namespace App\Controllers;
use CodeIgniter\Controller;
class Game extends BaseController
{
public function getListgames()
{
$session=session();
$loggedIn = $session->get('logged_in');
$gameModel = new \App\Models\GamesModel();
$data['games'] = $gameModel->findAll();
$data['content'] = view('game/listgames', $data);
      if(!$loggedIn){
          echo view("templates/header", $data);
          echo view("templates/lateral", $data);
      }
      else{
          echo view("templates/header_logged", $data); 
          echo view("templates/lateral", $data);
      }
echo view("game/listgames", $data);
echo view("templates/footer", $data);
}
}