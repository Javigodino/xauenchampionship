<?php
namespace App\Controllers;

use CodeIgniter\Controller;

class Tournaments extends BaseController
{
    public function getListtournaments()
    {
        $tournamentsModel = new \App\Models\TournamentsModel();
        $session = session();
        $data['tournamentsList'] = $tournamentsModel->findAll();
        $data['content'] = view('tournaments/listtournaments', $data);
        if (!session('logged_in')) {
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
        } else {
            echo view("templates/header_logged", $data);
            echo view("templates/lateral", $data);
        }
        echo view('tournaments/listtournaments', $data);
        echo view('templates/footer', $data);
    }

    public function getTournament_details($id)
    {
        $tournamentModel = new \App\Models\TournamentsModel();
        $userTournamentModel = new \App\Models\UserTournamentModel();
        $data['tournamentInfo'] = $tournamentModel->find($id);
        $data['userRegistered'] = $userTournamentModel->where('Id_Tournament',$id)->findAll();
        $data['content'] = view('tournaments/tournament_details', $data);
        if (!session('logged_in')) {
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
        } else {
            echo view("templates/header_logged", $data);
            echo view("templates/lateral", $data);
        }
        echo view('tournaments/tournament_details', $data);
        echo view('templates/footer', $data);
    }

    public function getAddtournament()
    {
        $gameModel = new \App\Models\GamesModel();
        $data['games'] = $gameModel->findAll();
        $data['content'] = view('tournaments/addtournament', $data);
        $session=session();
        if (!session('logged_in')) {
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
            return redirect()->to('user/login');
        }
        else{
             echo view("templates/header_logged", $data); 
             echo view("templates/lateral", $data);

        }
        echo view('tournaments/addtournament', $data);
        echo view('templates/footer', $data);
    }
    public function postAddTournament()
    {
        $request = \Config\Services::request();
        $validation = \Config\Services::validation();

        $rules = [
            'game_id' => [
                'label' => 'Id_juego',
                'rules' => 'required'
            ],
            'image' => [
                'label' => 'Imagen',
                'rules' => [
                    'uploaded[image]',
                    'is_image[image]',
                    'mime_in[image,image/jpg,image/jpeg,image/gif,image/png,image/webp]'
                ],
            ],
            'nombre' => [
                'label' => 'Nombre',
                'rules' => 'required|alpha_space'
            ],
            'descripcion' => [
                'label' => 'Descripcion',
                'rules' => 'required|alpha_space'
            ],
            'fecha' => [
                'label' => 'Fecha',
                'rules' => 'required|valid_date'
            ],
            'hora' => [
                'label' => 'Hora',
                'rules' => 'required'
            ],
            'maxJug' => [
                'label' => 'Maximo Jugadores',
                'rules' => 'required|numeric'
                
            ],
            'costeInscrip' => [
                'label' => 'Coste Inscripcion',
                'rules' => 'required|numeric'
            ],
            'premio' => [
                'label' => 'Premio ',
                'rules' => 'required|alpha_space'
            ]

        ];
        $data = [];
        $session = session();

        if ($this->validate($rules)) {
            $imagen = $this->request->getFile('image');
            // Genera un nombre único para la imagen
            $nombreAleatorio = uniqid();

            // Obtiene la extensión del archivo cargado
            $extension = $imagen->getClientExtension();

            // Combina el nombre aleatorio con la extensión del archivo
            $nombreArchivo = $nombreAleatorio . '.' . $extension;
            $rutaDestino = env('uploadDirectory');

            $imagen->move($rutaDestino, $nombreArchivo);




            $imageT = $nombreArchivo; ///Cambiarlo solo nombreArchivo, con el uploadPrefix tambien en la vista (mirar el bucket)
            $gameT = $request->getPost('game_id');
            $nameT = $request->getPost('nombre');
            $descriptionT = $request->getPost('descripcion');
            $dateT = $request->getPost('fecha');
            $hourT = $request->getPost('hora');
            $maxPlayerT = $request->getPost('maxJug');
            $inscriptionT = $request->getPost('costeInscrip');
            $rewardT = $request->getPost('premio');
            $data = [
                'Name' => $nameT,
                'Description' => $descriptionT,
                'Game' => $gameT,
                'Logo' => $imageT,
                'Max_Players' => intval($maxPlayerT),
                'Reward' => $rewardT,
                'Tournament_Date' => $dateT,
                'Tournament_Hour' => $hourT,
                'Inscription_Cost' => intval($inscriptionT),
            ];
            $tournamentsModel = new \App\Models\TournamentsModel();
            if ($tournamentsModel->insert($data)) {

                return redirect()->to(base_url('tournaments/listtournaments'));
            } else {
                $data['errors'] = $validation->getErrors();
                $session->setFlashdata('msg', 'No se ha conseguido insertar');
                 return redirect()->to(base_url('tournaments/addtournament'));
            }
        } else {
            $data['errors'] = $validation->getErrors();
            $session->setFlashdata('msg', 'Hay algun campo invalido');
            return redirect()->to(base_url('tournaments/addtournament'));


        }


    }
    public function postregisterTournament($idTorneo, $nameUser)
    {
        $request = \Config\Services::request();

        $tournamentsModel = new \App\Models\TournamentsModel();
        $userModel = new \App\Models\UserModel();
        $session = session();
        $userTournamentModelComprobar = new \App\Models\UserTournamentModel();
        if (($userTournamentModelComprobar->find($idTorneo) && $userTournamentModelComprobar->find($nameUser))) {
            return "Ya estaba";
        } else {
            if ($tournamentsModel->find($idTorneo) && $userModel->find($nameUser)) {
                $data = [
                    'Id_Tournament' => $idTorneo,
                    'User_Name' => $nameUser,
                ];
                $userTournamentModel = new \App\Models\UserTournamentModel();
                $userTournamentModel->insert($data);
                return redirect()->to(base_url('/tournaments/tournament_details/' . $idTorneo));
            }
        }


    }
    public function getdeleteTournament($id)
    {
        $request = \Config\Services::request();

        $tournamentsModel = new \App\Models\TournamentsModel();
        $userTournamentModel = new \App\Models\UserTournamentModel();
        $session = session();

        if ($tournamentsModel->find($id)) {
            
            $encontrado = $userTournamentModel->where('Id_Tournament', $id)->findAll();
            foreach ($encontrado as $user):
                $userTournamentModel->delete($user->Indice_Registro);
            endforeach;
            $tournamentsModel->delete($id);
            return redirect()->to(base_url('tournaments/listtournaments'));
        }
    }
    public function getderegister($id, $user)
    {
        $request = \Config\Services::request();

        $userTournamentModel = new \App\Models\UserTournamentModel();
        $session = session();
        $encontrado = $userTournamentModel->where('User_Name', $user)
            ->where('Id_Tournament', $id)
            ->first();
        $userTournamentModel->delete($encontrado->Indice_Registro);
        return redirect()->to(base_url('/tournaments/tournament_details/' . $id));

    }

}