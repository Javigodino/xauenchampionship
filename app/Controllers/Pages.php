<?php
namespace App\Controllers;
class Pages extends BaseController
{
    public function getIndex()
    {
        return view("welcome_message");
    }
    public function getView($page = "home")
    {
        if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);
        if(!session('logged_in')){
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
        }
        else{
             echo view("templates/header_logged", $data); 
             echo view("templates/lateral", $data);
        }
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
    }
    


    public function getTorneo($page = "torneo")

    {
        if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);        
        echo view("templates/header", $data);
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
    }

    public function getCrearTorneo($page = "crearTorneo")
    {
        if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);        
        echo view("templates/header_logged", $data);
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
    }

    public function getInfoTorneo($page = "infoTorneo")
    {
        if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);        
        echo view("templates/header", $data);
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
    }
    


}

