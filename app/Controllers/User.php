<?php
namespace App\Controllers;
use CodeIgniter\Controller;
class User extends BaseController
{
	protected $helpers = ['url', 'form'];


	public function getLogin($page = "login")
    {
        if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);        
        echo view("templates/header", $data);
        echo view("templates/lateral", $data);
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
	}
	
	 public function getRegistro($page = "registro")
    {
        if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);        
        echo view("templates/header", $data);
		echo view("templates/lateral", $data);
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
    }

	 public function getDatos($page = "datos")
    {
        $game = new \App\Models\GamesModel();
        $data['games']=$game->findAll();
         if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);        
		if(!session('logged_in')){
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
            return redirect()->to('user/login');
        }
        else{
             echo view("templates/header_logged", $data); 
             echo view("templates/lateral", $data);
        }
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
    }


	
    public function getPersonal($page = "personal"){
        if (! is_file(APPPATH.'Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
        $data['title'] = ucfirst($page);        
		if(!session('logged_in')){
            echo view("templates/header", $data);
            echo view("templates/lateral", $data);
        }
        else{
             echo view("templates/header_logged", $data); 
             echo view("templates/lateral", $data);
        }
        echo view("pages/".$page, $data);
        echo view("templates/footer", $data);
    }

	public function postLogin()
	{
		$request = \Config\Services::request();
		$validation = \Config\Services::validation();
		$rules = [
		'user' => [
		'label' => 'user:',
		'rules' => 'required'
		//"rules" => "required|min_length[3]|max_length[20]|valid_email|is_unique[user.email]"
		],
		'password' => [
		'label' => 'password:',
		'rules' => 'required'
		//"rules" => "required|min_length[8]|max_length[20]"
		]
		];
		$data = [];
		$session = session();
		if ($this->validate($rules)) {
			$user = $request->getVar('user');
			$password = $request->getVar('password');
			$usuario = model('UserModel')->authenticate($user, $password);
			if ($usuario) {
				$session->set('logged_in', TRUE);
				$session->set('user', $usuario);
				return redirect()->to(base_url());
			} else {
				$session->setFlashdata('msg', 'Credenciales incorrectas');
				return redirect()->to('user/login');
			}
		}else {
			$session->setFlashdata('msg', 'Fallo al iniciar sesion.');
			return redirect()->to('user/login');
		}
		
	}

	public function getLogout()
	{
		$session=session();
		$session->set('logged_in', FALSE);
		session()->destroy();
		return redirect()->to(base_url());
	}

	public function postRegistro(){
		$request = \Config\Services::request();
        $validation = \Config\Services::validation();

		$rules = [
            'nombre' => [
                'label' => 'name',
                'rules' => 'required|min_length[3]|max_length[15]|is_unique[user.name]'
            ],
            'password' => [
                'label' => 'password',
                'rules' => 'required|min_length[8]|max_length[255]|regex_match[/^(?=.*\d)(?=.*[!@#$%^&*()-_=+{};:,.<>?])(?=.*[a-zA-Z]).{8,}$/]|alpha_space',
			],
				
            'password2' => [
                'label' => 'password2',
                'rules' => 'required|min_length[8]|max_length[255]|regex_match[/^(?=.*\d)(?=.*[!@#$%^&*()-_=+{};:,.<>?])(?=.*[a-zA-Z]).{8,}$/]|alpha_space',
			]
        ];

		

		$data = [];
        $session = session();

		if ($this->validate($rules)) {
            
            $nameU = $request->getPost('nombre');
            $password = $request->getPost('password');
            $password2 = $request->getPost('password2');
            
			if($password == $password2){
				$data = [
                'name' => $nameU,
                'password' => password_hash($password,PASSWORD_DEFAULT),
				'rol' => '1'
				];
				$userModel = new \App\Models\UserModel();
				$userModel->insert($data);
				return redirect()->to('user/login');
			}else {

				$session->setFlashdata('msg', 'Contraseñas incorrectas.');
				return redirect()->to('user/registro');

			}
            
        } else {
            $session->setFlashdata('msg', 'La contraseña debe contener al menos un número y al menos un signo de puntuación y usar mínimo 8 caracteres.');
			return redirect()->to('user/registro');

        }
      
	}

    public function postAgregarJuego($id){
		$request = \Config\Services::request();
        $validation = \Config\Services::validation();
        $game = new \App\Models\GamesModel();
        
		$data = [];
        $session = session();
        $data['games']=$game->findAll();
        $nameU = $session->get('user')->name;
        
        if(!$game->find($id)){

            return redirect()->to(base_url('user/datos'));;

        }else{
            $data = [
                'user' => $nameU,
                'game' => $id,
            ];
            $userGame = new \App\Models\GamesRentedModel();
            $userGame->insert($data);
            return redirect()->to(base_url('user/datos'));
        }
			
      
	}

    public function getborrar($idJuego)
    {
        $request = \Config\Services::request();
        $gameRentedModel = new \App\Models\GamesRentedModel();
        $session = session();
        $nameU = $session->get('user')->name;
        $encontrado = $gameRentedModel->where('user',$nameU)
            ->where('game', $idJuego)
            ->first();
        $gameRentedModel->delete($encontrado->id);
        return redirect()->to(base_url('user/datos'));

    }


}

