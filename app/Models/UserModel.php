<?php
namespace App\Models;
use CodeIgniter\Model;
class UserModel extends Model
{
	protected $table = 'user';
	protected $primaryKey = 'name';
	protected $useAutoIncrement = true; # La base de datos se encarga de ello
	protected $returnType = 'object'; # 'object' o 'array'
	protected $useSoftDeletes = false; # true si pretendes recuperar datos
	# Campos que permiten operaciones CRUD
	protected $allowedFields = ['name', 'password','rol'];
	# no guardar marcas de tiempo con los "insert" y "update"
	protected $useTimestamps = false;
	 
	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;

	public function authenticate($user, $password)
	{
		$usuario = $this->find($user);
		
		if ($usuario && (password_verify($password,$usuario->password)))
		{
			return $usuario;
		}
		return false;
	}
}
