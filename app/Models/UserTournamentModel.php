<?php
namespace App\Models;

use CodeIgniter\Model;

class UserTournamentModel extends Model
{
    protected $table = 'user_tournament';
   protected $primaryKey = 'Indice_Registro';
    protected $useAutoIncrement = true; # La base de datos se encarga de ello
    protected $returnType = 'object'; # 'object' o 'array'
    protected $useSoftDeletes = false; # true si pretendes recuperar datos
# Campos que permiten operaciones CRUD
    protected $allowedFields = ['Id_Tournament','User_Name'];
    # no guardar marcas de tiempo con los "insert" y "update"
    protected $useTimestamps = false;
    # No utilizar reglas de validación (por el momento...)
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}