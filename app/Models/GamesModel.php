<?php
namespace App\Models;
use CodeIgniter\Model;
class GamesModel extends Model
{
protected $table = 'games';
protected $primaryKey = 'Name';
protected $useAutoIncrement = false; # La base de datos se encarga de ello
protected $returnType = 'object'; # 'object' o 'array'
protected $useSoftDeletes = false; # true si pretendes recuperar datos
# Campos que permiten operaciones CRUD
protected $allowedFields = ['Name', 'Description', 'NumPlayers', 'Logo', 'url_download'];
# no guardar marcas de tiempo con los "insert" y "update"
protected $useTimestamps = false;
# No utilizar reglas de validación (por el momento...)
protected $validationRules = [];
protected $validationMessages = [];
protected $skipValidation = false;
}