<?php
namespace App\Models;
use CodeIgniter\Model;
class DeviceRentedModel extends Model
{
protected $table = 'devices_user';
protected $primaryKey = 'id_device';
protected $useAutoIncrement = true; # La base de datos se encarga de ello
protected $returnType = 'object'; # 'object' o 'array'
protected $useSoftDeletes = false; # true si pretendes recuperar datos
# Campos que permiten operaciones CRUD
protected $allowedFields = ['id_device', 'username'];
# no guardar marcas de tiempo con los "insert" y "update"
protected $useTimestamps = false;
# No utilizar reglas de validación (por el momento...)
protected $validationRules = [];
protected $validationMessages = [];
protected $skipValidation = false;
}