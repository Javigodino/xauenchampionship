<?php
namespace App\Models;

use CodeIgniter\Model;

class GamesRentedModel extends Model
{
    protected $table = 'user_games';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true; # La base de datos se encarga de ello
    protected $returnType = 'object'; # 'object' o 'array'
    protected $useSoftDeletes = false; # true si pretendes recuperar datos
# Campos que permiten operaciones CRUD
    protected $allowedFields = ['user', 'game'];
    # no guardar marcas de tiempo con los "insert" y "update"
    protected $useTimestamps = false;
    # No utilizar reglas de validaci�n (por el momento...)
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}