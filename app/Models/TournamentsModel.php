<?php
namespace App\Models;

use CodeIgniter\Model;

class TournamentsModel extends Model
{
    protected $table = 'tournaments';
    protected $primaryKey = 'Id_Tournament';
    protected $useAutoIncrement = true; # La base de datos se encarga de ello
    protected $returnType = 'object'; # 'object' o 'array'
    protected $useSoftDeletes = false; # true si pretendes recuperar datos
# Campos que permiten operaciones CRUD
    protected $allowedFields = ['Id_Tournament','Name', 'Description', 'Logo','Max_Players','Reward','Tournament_Date','Tournament_Hour',
    'Inscription_Cost','Game'];
    # no guardar marcas de tiempo con los "insert" y "update"
    protected $useTimestamps = false;
    # No utilizar reglas de validación (por el momento...)
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}