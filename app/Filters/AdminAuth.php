<?php
namespace App\Filters;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
class AdminAuth implements FilterInterface
	{		public function before(RequestInterface $request, $arguments = null)
	{
		if (!session('logged_in'))
			return redirect()->to(base_url('pages/home'));
		elseif ((session('user')->role & 2) == 0)
			return redirect()->to(base_url('user/unauthorized'));
	}	
	public function after(RequestInterface $request, ResponseInterface $response,
$arguments = null) {}
}