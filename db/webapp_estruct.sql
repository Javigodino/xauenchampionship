-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2024 a las 11:17:00
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `webapp`
--
-- DROPS TABLES
DROP TABLE IF EXISTS `devices_user`;
DROP TABLE IF EXISTS `user_games`;
DROP TABLE IF EXISTS `user_tournament`;
DROP TABLE IF EXISTS `tournaments`;
DROP TABLE IF EXISTS `devices`;
DROP TABLE IF EXISTS `games`;
DROP TABLE IF EXISTS `user`;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices`
--


CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(1500) NOT NULL,
  `Logo` varchar(255) NOT NULL,
  `Rented` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices_user`
--


CREATE TABLE `devices_user` (
  `id_device` int(10) UNSIGNED NOT NULL,
  `username` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `games`
--


CREATE TABLE `games` (
  `Name` varchar(255) NOT NULL,
  `Description` varchar(1500) NOT NULL,
  `NumPlayers` int(11) NOT NULL,
  `Logo` varchar(255) NOT NULL,
  `url_download` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tournaments`
--


CREATE TABLE `tournaments` (
  `Id_Tournament` int(255) UNSIGNED NOT NULL,
  `Name` varchar(1500) NOT NULL,
  `Description` varchar(1500) NOT NULL,
  `Logo` varchar(255) NOT NULL,
  `Max_Players` int(255) NOT NULL,
  `Reward` varchar(255) NOT NULL,
  `Tournament_Date` date NOT NULL,
  `Tournament_Hour` varchar(255) NOT NULL,
  `Inscription_Cost` int(255) NOT NULL,
  `Game` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--


CREATE TABLE `user` (
  `name` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_games`
--


CREATE TABLE `user_games` (
  `id` int(11) NOT NULL,
  `user` varchar(15) NOT NULL,
  `game` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_tournament`
--


CREATE TABLE `user_tournament` (
  `Id_Tournament` int(255) UNSIGNED NOT NULL,
  `User_Name` varchar(15) NOT NULL,
  `Indice_Registro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `devices_user`
--
ALTER TABLE `devices_user`
  ADD PRIMARY KEY (`id_device`),
  ADD KEY `FK_devices` (`id_device`),
  ADD KEY `FK_user` (`username`);

--
-- Indices de la tabla `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`Name`);

--
-- Indices de la tabla `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`Id_Tournament`),
  ADD KEY `FK_Games` (`Game`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `user_games`
--
ALTER TABLE `user_games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_game` (`game`),
  ADD KEY `fk_user` (`user`);

--
-- Indices de la tabla `user_tournament`
--
ALTER TABLE `user_tournament`
  ADD PRIMARY KEY (`Indice_Registro`),
  ADD KEY `FK_Tournament` (`Id_Tournament`),
  ADD KEY `FK_User` (`User_Name`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `Id_Tournament` int(255) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_games`
--
ALTER TABLE `user_games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_tournament`
--
ALTER TABLE `user_tournament`
  MODIFY `Indice_Registro` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `devices_user`
--
ALTER TABLE `devices_user`
  ADD CONSTRAINT `devices_user_ibfk_1` FOREIGN KEY (`id_device`) REFERENCES `devices` (`id`),
  ADD CONSTRAINT `devices_user_ibfk_2` FOREIGN KEY (`username`) REFERENCES `user` (`name`);

--
-- Filtros para la tabla `tournaments`
--
ALTER TABLE `tournaments`
  ADD CONSTRAINT `tournaments_ibfk_1` FOREIGN KEY (`Game`) REFERENCES `games` (`Name`);

--
-- Filtros para la tabla `user_games`
--
ALTER TABLE `user_games`
  ADD CONSTRAINT `user_games_ibfk_1` FOREIGN KEY (`game`) REFERENCES `games` (`Name`),
  ADD CONSTRAINT `user_games_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`name`);

--
-- Filtros para la tabla `user_tournament`
--
ALTER TABLE `user_tournament`
  ADD CONSTRAINT `user_tournament_ibfk_1` FOREIGN KEY (`Id_Tournament`) REFERENCES `tournaments` (`Id_Tournament`),
  ADD CONSTRAINT `user_tournament_ibfk_2` FOREIGN KEY (`User_Name`) REFERENCES `user` (`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
