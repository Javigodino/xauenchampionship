-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2024 a las 11:22:49
-- Versión del servidor: 10.4.32-MariaDB
-- Versión de PHP: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `webapp`
--

--
-- Volcado de datos para la tabla `devices`
--

INSERT INTO `devices` (`id`, `Name`, `Description`, `Logo`, `Rented`) VALUES
(1, 'Monitor', 'ASUS VG258QM TUF, monitor de 24.5\" Full HD (1920 x 1080) con una frecuencia de refresco de 280 Hz para gamers profesionales que buscan una experiencia gaming inmersiva.\r\nEl tiempo de respuesta GTG de 0.5 ms produce unas imágenes nítidas y frame rates altos.\r\n', '/img/monitor.jpg', 0),
(2, 'Raton', 'Cambia rápidamente de PPP mediante los botones de aumento/reducción (800/1200/1600/2000/2400 PPP)\r\n3 botones adicionales para el pulgar: avance, retroceso y CTRL\r\nPráctico botón de doble disparo con un solo clic\r\nPosibilidad de apagar o encender la Iluminación por LED mediante el Interruptor de encendido\r\nConexión inalámbrica estable permanente hasta a 8 m', '/img/raton.jpg', 0),
(3, 'Teclado', 'Cada botón del teclado mecánico AK33 tiene un interruptor independiente para controlar, y las pulsaciones de las teclas son fuertes, dando como resultado una sensación especial para el entretenimiento del juego. Hay dos tipos de interruptores para elegir, el azul y el negro.\r\nEl teclado blanco tiene una luz de fondo azul única, y el teclado negro tiene una luz de fondo blanca, que es muy adecuado para el uso nocturno. Diseño de cable USB independiente, todo para la comodidad del reproductor para usar y transportar.', '/img/teclado.jpg', 0),
(17, 'cascos', 'Ultra casco Super Guay', '/img/cascos.jpg', 0);

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`name`, `password`, `rol`) VALUES
('CarlosTrooper', '$2y$10$x0xT3G/X92/glKgg5DblKOhxYcTaJLAbthLq7VbE/olldVGTxNn/i', 2),
('Fran', '$2y$10$Ew91lwWoN71hWox7dG8I2On1wjOVy1IHU2Caj4lrO6nc5IaNGI6mm', 1),
('JaviGodino', '$2y$10$np5wTQujUufmVt9F68.i8uUE35yQlFoxy.juyT4uRqcJ/goo8HQVO', 1);

--
-- Volcado de datos para la tabla `games`
--

INSERT INTO `games` (`Name`, `Description`, `NumPlayers`, `Logo`, `url_download`) VALUES
('Counter Strike 2', 'Counter-Strike 2 es un shooter táctico multijugador en primera persona, donde dos equipos compiten para completar diferentes objetivos, según el modo de juego seleccionado. Los jugadores se dividen en dos equipos, los antiterroristas (CT) y los terroristas (T). La mayoría de los modos de juego se dividen en varias rondas y, entre rondas, los jugadores pueden comprar diferentes armas y equipos para usar. En la mayoría de los modos de juego, los jugadores tienen una sola vida por ronda y, si mueren, no podrán jugar hasta el comienzo de la siguiente ronda.', 5, '/img/counterstrike2.jpg', 'https://www.counter-strike.net/cs2'),
('EA FC 24', 'Simulador de fútbol. Podrás competir con tus equipos de Ultimate Team ya sea en modalidad individual o por parejas. Forma tus mejores equipos y podrás conseguir grandes recompensas', 1, '/img/eafc.jpg', 'https://www.ea.com/es-es/games/ea-sports-fc/fc-24/buy/pc'),
('League of Legends', 'League of Legends es un juego de arena de batalla multijugador en línea (MOBA) en el que el jugador controla un personaje (\"campeón\") con un conjunto de habilidades únicas desde una perspectiva isométrica. A julio de 2022 había más de 150 campeones disponibles para jugar. En el transcurso de una partida, los campeones ganan niveles al acumular puntos de experiencia (XP) al matar enemigos o personajes no jugadores (NPCs). Los elementos se pueden adquirir para aumentar la fuerza de los campeones, y se compran con oro, que los jugadores acumulan pasivamente con el tiempo y ganan activamente al derrotar a los súbditos, campeones o estructuras defensivas del equipo contrario. En los modos principales de juego los artículos se compran a través de un menú de tienda disponible para los participantes únicamente cuando su campeón está en la base del equipo. Cada partida es única, de modo que los niveles y los elementos comprados no se transfieren de una partida a otra.', 5, '/img/lol.jpg', 'https://signup.leagueoflegends.com/es-es/signup/redownload'),
('Pokemon', 'Los jugadores arman un equipo de Pokémon, el cual debe definirse según un conjunto de reglas concreto, y combaten de la forma en la que lo harían en el juego, o sea, hasta que todos los Pokémon en el equipo de un jugador se hayan debilitado, o también has', 1, '/img/pkmn.jpg', 'https://www.game.es/pokemon-escarlata-nintendo-switch-200227'),
('Valorant', 'Valorant es un hero shooter en primera persona ambientado en un futuro próximo. Los jugadores asumen el control de agentes, personajes que provienen de una gran cantidad de países y culturas de todo el mundo. En el modo de juego principal, los jugadores se unen al equipo atacante o defensor con cada equipo que tiene cinco jugadores. Los agentes tienen habilidades únicas y usan un sistema económico para comprar sus habilidades y armas. El videojuego tiene una variedad de armas que incluyen pistolas, subfusiles, escopetas, ametralladoras, fusiles de asalto y fusiles de francotirador y makul. Cada arma tiene un patrón de retroceso que debe ser controlado por el jugador para poder disparar con precisión. El equipo atacante tiene una bomba, llamada Spike, que necesitan plantar en un sitio. Si el equipo atacante protege con éxito la Spike durante 40 segundos y detona, obtendrán un punto. Si el equipo defensor desactiva con éxito la Spike, o el temporizador de la ronda de 100 segundos expira, el equipo defensor obtiene un punto. Si se eliminan todos los miembros de un equipo, el equipo contrario gana un punto. Después de doce rondas, el equipo atacante cambia al equipo defensor y viceversa. El primer equipo en ganar 13 rondas gana la partida. Exceptuando el tiempo extra, donde deberás conseguir 2 victorias/rondas seguidas.', 5, '/img/valorant.jpg', 'https://playvalorant.com/es-es/?gad_source=1&gclid=EAIaIQobChMI0rDA__TfhAMVXJSDBx3rUgOKEAAYASAAEgISnvD_BwE&gclsrc=aw.ds');

--
-- Volcado de datos para la tabla `devices_user`
--

INSERT INTO `devices_user` (`id_device`, `username`) VALUES
(3, 'JaviGodino');


--
-- Volcado de datos para la tabla `tournaments`
--

INSERT INTO `tournaments` (`Id_Tournament`, `Name`, `Description`, `Logo`, `Max_Players`, `Reward`, `Tournament_Date`, `Tournament_Hour`, `Inscription_Cost`, `Game`) VALUES
(1, 'Torneo Local de Pokemon Escarlata y Púrpura', 'Participa en nuestro Torneo Pokémon!¿Tienes lo necesario para convertirte en el mejor entrenador? ¡Demuéstralo en nuestro torneo! Grandes premios te esperan.¡Prepara a tus Pokémon y sé parte de la acción! No te lo pierdas.', 'pkmn.jpg', 25, '100 euros en metalico', '2024-05-06', '16:30 h (ESP)', 7, 'Pokemon'),
(2, 'Torneo Fifa 23/24 escolar', '¡Participa en nuestro Torneo de FIFA!\r\n¿Tienes lo necesario para convertirte en el campeón virtual? ¡Demuéstralo en nuestro torneo y gana increíbles premios!', 'eafc.jpg', 25, 'Un jamón', '2024-05-22', '16:30 Hora española', 5, 'EA FC 24'),
(3, 'Torneo csgo 23/24 escolar', 'Forma tu equipo, afina tu puntería y prepárate para la intensidad táctica de CSGO. Con premios emocionantes en juego, esta es tu oportunidad de demostrar tus habilidades y dejar tu marca en el mundo de los esports.', 'counterstrike2.jpg', 35, '20€ en Steam', '2024-06-22', '22:30 Hora española', 5, 'Counter Strike 2');


--
-- Volcado de datos para la tabla `user_tournament`
--

INSERT INTO `user_tournament` (`Id_Tournament`, `User_Name`, `Indice_Registro`) VALUES
(1, 'JaviGodino', 6),
(2, 'JaviGodino', 7),
(1, 'Fran', 17),
(3, 'Fran', 18);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
