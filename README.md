# Aspectos generales del proyecto


## ¿De que trata la aplicación?
Nuestra aplicación trata de un sistema de gestión de torneos relacionados con videojuegos, dichos torneos son creados por administradores y te otorgarán diferentes premios.

## Funcionalidades que puedes encontrar
- Registro.

Cualquier persona que entre en la página, podrá crear una cuenta y poder acceder a los diferentes torneos que ofrece la página.

- Torneos.

En nuestra aplicación, un nuevo usuario puede registrarse y poder participar en los torneos que se encuentren ya creados.
En el caso de que un usuario no disponga de un juego y quiera apuntarse a un torneo de dicho juego, la pagina le da la opcion de acceder a alguna plataforma en la que adquirir dicho juego para que pueda participar.

Un usuario podrá llevar el recuento en la pantalla de datos de aquellos juegos que posee y pueden aparecer torneos de este. En el caso de que el usuario borre el juego, podrá eliminarlo de su lista. Tambien podrá añadir a su lista personal si ha adquirido algun juego que no tenía previamente.
- Dispositivos.

Si un usuario necesita un dispositivo ya sea (cascos, monitor, teclado o ratón), encontrará un apartado donde reservar uno de ellos. Si el usuario ha terminado de utilizar dicho dispositivo, le da la opción de devolverlo.

Un dispositivo, mientras se encuentre reservado por un usuario no será posible alquilarlo ni eliminarlo de la base de datos. Será necesario que el usuario devuelva dicho dispositivo para que se pueda eliminar de la base de datos.

En el caso de que algun usuario disponga de algun dispositivo extra, ya sea 2 monitores, cascos, teclados o ratones, este podría añadirlo a dispositivos a reservar y que cualquier otro usuario de la página lo reserve.

## Roles de usuarios

En nuestra página podrá encontrar 2 roles diferentes de usuarios: **Administrador** y **Usuario**

Una cuenta que tenga rol **Usuario**, podrá llevar el registro de los juegos que dispone, apuntarse a torneos, reservar y devolver dispositivos e incluir un nuevo dispositivo en la lista de dispositivos para reservar.

En el caso de que la cuenta posea del rol **Administrador**, este podrá crear nuevos torneos, eliminar torneos ya existentes y borrar dispositivos de la lista de dispositivos para reservar.

## Informacion que puedes encontrar en cada sección
- Página principal.

En la página principal podrá ver una previsualización de los diferentes juegos que puede encontrar torneos.

- Página Registro.

Esta página es la encargada de comprobar si un usuario tiene o no cuenta registrada en el sistema. Si un usuario no esta registrado, podrá crearse un nuevo usuario.

- Página juegos.

En esta parte podrá obtener información acerca del juego que desee. Podrá ver una imagen identificativa, una descripción de en que consiste el juego y como son los torneos de dicho juego y por ultimo, un botón donde acceder a una dirección donde poder adquirir dicho juego.

- Página torneos.

En este apartado aparecerá la lista de los diferentes torneos a los que puede apuntarse un usuario.
En ella se podrá ver la imagen del juego al que pertenece y una breve descripción.

- Página infotorneo.

Esta será la página en la que poder ver la información de todo el torneo, desde el titulo del torneo, descripción del mismo, fecha y hora a la que se celebrará, precio de las inscripciones, premio y usuarios inscritos en dicho torneo.

- Página periféricos.

Aquí aparecen una lista de los diferentes dispositivos que se puede reservar en nuestra página. 

- Página datos.

En esta parte se podrá ver los diferentes juegos que hay registrados en la página. Aquí cada usuario podrá llevar el recuento de juegos que posee, añadiendo o borrando de su lista aquellos que desee.

- Página sobre nosotros.

Esta es una página donde vera una breve información de los desarrolladores de la página web. 

Encontrara una foto, el correo para contactar y una breve descripción sobre cada componente del grupo.

# Aspectos generales del proyecto
## ¿De que trata la aplicación?
Nuestra aplicación trata de un sistema de gestión de torneos relacionados con videojuegos, dichos torneos son creados por administradores y te otorgarán diferentes premios.

## Funcionalidades que puedes encontrar
- Registro.

Cualquier persona que entre en la página, podrá crear una cuenta y poder acceder a los diferentes torneos que ofrece la página.

- Torneos.

En nuestra aplicación, un nuevo usuario puede registrarse y poder participar en los torneos que se encuentren ya creados.
En el caso de que un usuario no disponga de un juego y quiera apuntarse a un torneo de dicho juego, la pagina le da la opcion de acceder a alguna plataforma en la que adquirir dicho juego para que pueda participar.

Un usuario podrá llevar el recuento en la pantalla de datos de aquellos juegos que posee y pueden aparecer torneos de este. En el caso de que el usuario borre el juego, podrá eliminarlo de su lista. Tambien podrá añadir a su lista personal si ha adquirido algun juego que no tenía previamente.
- Dispositivos.

Si un usuario necesita un dispositivo ya sea (cascos, monitor, teclado o ratón), encontrará un apartado donde reservar uno de ellos. Si el usuario ha terminado de utilizar dicho dispositivo, le da la opción de devolverlo.

Un dispositivo, mientras se encuentre reservado por un usuario no será posible alquilarlo ni eliminarlo de la base de datos. Será necesario que el usuario devuelva dicho dispositivo para que se pueda eliminar de la base de datos.

En el caso de que algun usuario disponga de algun dispositivo extra, ya sea 2 monitores, cascos, teclados o ratones, este podría añadirlo a dispositivos a reservar y que cualquier otro usuario de la página lo reserve.

## Roles de usuarios

En nuestra página podrá encontrar 2 roles diferentes de usuarios: **Administrador** y **Usuario**

Una cuenta que tenga rol **Usuario**, podrá llevar el registro de los juegos que dispone, apuntarse a torneos, reservar y devolver dispositivos e incluir un nuevo dispositivo en la lista de dispositivos para reservar.

En el caso de que la cuenta posea del rol **Administrador**, este podrá crear nuevos torneos, eliminar torneos ya existentes y borrar dispositivos de la lista de dispositivos para reservar.

## Informacion que puedes encontrar en cada sección
- Página principal.

En la página principal podrá ver una previsualización de los diferentes juegos que puede encontrar torneos.

- Página Registro.

Esta página es la encargada de comprobar si un usuario tiene o no cuenta registrada en el sistema. Si un usuario no esta registrado, podrá crearse un nuevo usuario.

- Página juegos.

En esta parte podrá obtener información acerca del juego que desee. Podrá ver una imagen identificativa, una descripción de en que consiste el juego y como son los torneos de dicho juego y por ultimo, un botón donde acceder a una dirección donde poder adquirir dicho juego.

- Página torneos.

En este apartado aparecerá la lista de los diferentes torneos a los que puede apuntarse un usuario.
En ella se podrá ver la imagen del juego al que pertenece y una breve descripción.

- Página infotorneo.

Esta será la página en la que poder ver la información de todo el torneo, desde el titulo del torneo, descripción del mismo, fecha y hora a la que se celebrará, precio de las inscripciones, premio y usuarios inscritos en dicho torneo.

- Página periféricos.

Aquí aparecen una lista de los diferentes dispositivos que se puede reservar en nuestra página. 

- Página datos.

En esta parte se podrá ver los diferentes juegos que hay registrados en la página. Aquí cada usuario podrá llevar el recuento de juegos que posee, añadiendo o borrando de su lista aquellos que desee.

- Página sobre nosotros.

Esta es una página donde vera una breve información de los desarrolladores de la página web. Encontrara una foto, el correo para contactar y una breve descripción sobre cada componente del grupo.
