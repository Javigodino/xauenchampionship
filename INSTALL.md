# INSTRUCCIONES PARA DESPLEGAR LA APLICACIÓN WEB
## Asegurarse que TODO este en su sitio
1. Para ello debemos asegurarnos que la rama que vamos a desplegar sea la rama Master de nuestro proyecto en gitlab. Para ello usaremos el comando de git git clone [URL del proyecto en git], ya que deberemos cambiar ciertas cosas.
2. Ademas previamente debemos tener creada la maquina en gcloud como ya viene dictado en el guion 9.-Despliegue en G-Cloud (El guión de referencia se encuentra dentro del proyecto)
3. Posteriormente asegurarnos que tenemos creada la instancia de base de datos creada en la maquina.

- Recuerda que al crear todo esto se proporcionarán una serie de datos que van a ser imprescindibles para realizar un correcto despliegue.

## Preparar los archivos
- ¿Recordamos los datos anteriores? Perfecto, porque ahora te van a hacer falta. Hora de cambiar el .env de la aplicacion web.
1. Tendrás que cambiar ciertas lineas en el archivo relacionados con la base de datos (database.default.*):
- - **Hostname**: Deberemos poner la direccion IP donde está alojado nuestra maquina junto con la instancia de la base de datos.
- - **Database**: Deberemos introducir el nombre que le hemos puesto a la base de datos al momento de crearla.
- - **Username**: Necesitaremos poner un usuario que hayamos creado para poder realizar los accesos a la BD, (recomendable no poner root)
- - **Password**: Al crear el usuario tambien debe estar asociada una contraseña (lo más recomendable es que sea autogenerada, y que la guardamos en un sitio seguro junto a resto de datos)
- - **DBDriver**: Indicar basicamente el tipo de base de datos que estamos usando (en nuestro caso será MySQLi)

2. Tambien debemos establecer varios parámetros en este archivo:
- - Configurar la BaseURL: En vez de localhost, debemos poner la URL real de la app "https://...."  (tambien se proporciona a la hora de crear la instancia)

3. Hora de hacer mas cambios, ahora en el Config, que simplemente seguiremos los pasos del guion 8., TAL CUAL ESTAN ESCRITOS EN EL GUIÓN (en el proyecto hay dos ficheros de Config, uno para despliegue que en la ram master sera Config, y otro para desarrollo Config.dev)

## Configurar el almacenimiento de imagenes por buckets
- Bueno, ya que nuestra aplicacion hace uso de carga de imágenes y almacenimiento de las mismas, vamos a preparar el entorno para ello.
1. **Creacion de buckets:**
- - Abriremos nuestro almacenimiento en la nube y crearemos (dentro del bucket que tenemos de la app) un bucket al cual llamaremos "img/".
- - Una vez que tengamos el bucket creado, debemos seguir los pasos del guion 8.2 y darle los permisos pertinentes.
- - Ahora tendremos que subir la carpeta que se encuentra en public/img, que contiene las imagenes necesarias para la funcionalidad de torneo (si la base de datos no se va a cargar con datos iniciales pues este paso no haría falta, aunque es muy recomendable )

2. **Adapatar el .env**
- - Ultima vez que cambiamos el .env (lo prometo), para poder haccer uso de estos buckets, hay que configurar dos lineas, la de uploadDirectory y uploadPrefix , ya que en vez de las carpetas de localHost hay que cambiar por el "gs://...", y el "https://..." correspondiente al de almacenimiento en el bucket.
- - Si hay duda de como es la sintaxis, es de la misma forma que en el guion 8.2 pero con el nombre que hayamos elegido para la app.
- - Ademas de cambiar el CI_ENVIRONMENT a desarrollo (development)

3. **Cuidado con los composer**
- - Este error puede ocurrir a la hora de como queda la app, si se tiene un composer.lock puede ser que bloquee las bibliotecas y no se efectua correctamente
- - El aspecto del composer debía ser parecido a este (sin embargo, puede que haya actualizaciones):
````
{
    "name": "codeigniter4/framework",
    "description": "The CodeIgniter framework v4",
    "license": "MIT",
    "type": "project",
    "homepage": "https://codeigniter.com",
    "support": {
        "forum": "https://forum.codeigniter.com/",
        "source": "https://github.com/codeigniter4/CodeIgniter4",
        "slack": "https://codeigniterchat.slack.com"
    },
    "require": {
        "php": "^7.4 || ^8.0",
        "ext-json": "*",
        "ext-intl": "*",
        "ext-mbstring": "*",
        "google/cloud-storage": "*",
        "laminas/laminas-escaper": "^2.9",
        "psr/log": "^1.1"
    },
    "require-dev": {
        "codeigniter/coding-standard": "^1.7",
        "fakerphp/faker": "^1.9",
        "friendsofphp/php-cs-fixer": "^3.47.1",
        "kint-php/kint": "^5.0.4",
        "mikey179/vfsstream": "^1.6",
        "nexusphp/cs-config": "^3.6",
        "phpunit/phpunit": "^9.1",
        "predis/predis": "^1.1 || ^2.0"
    },
    "suggest": {
        "ext-curl": "If you use CURLRequest class",
        "ext-dom": "If you use TestResponse",
        "ext-exif": "If you run Image class tests",
        "ext-fileinfo": "Improves mime type detection for files",
        "ext-gd": "If you use Image class GDHandler",
        "ext-imagick": "If you use Image class ImageMagickHandler",
        "ext-libxml": "If you use TestResponse",
        "ext-memcache": "If you use Cache class MemcachedHandler with Memcache",
        "ext-memcached": "If you use Cache class MemcachedHandler with Memcached",
        "ext-mysqli": "If you use MySQL",
        "ext-oci8": "If you use Oracle Database",
        "ext-pgsql": "If you use PostgreSQL",
        "ext-readline": "Improves CLI::input() usability",
        "ext-redis": "If you use Cache class RedisHandler",
        "ext-simplexml": "If you format XML",
        "ext-sodium": "If you use Encryption SodiumHandler",
        "ext-sqlite3": "If you use SQLite3",
        "ext-sqlsrv": "If you use SQL Server",
        "ext-xdebug": "If you use CIUnitTestCase::assertHeaderEmitted()"
    },
    "autoload": {
        "psr-4": {
            "CodeIgniter\\": "system/"
        },
        "exclude-from-classmap": [
            "**/Database/Migrations/**"
        ]
    },
    "config": {
        "optimize-autoloader": true,
        "preferred-install": "dist",
        "sort-packages": true
    },
    "scripts": {
        "test": "phpunit"
    }
}
````

Con todo esto su aplicacion debería de tener perfecto acceso a su bucket.

## Crear la base de datos
Dentro del proyecto, hay una carpeta llamada db, donde se almacenarán los dos script sql para iniciar la base de datos
- webapp_struct: que contiene la estructura de las tablas para su creacion
- webapp_data: que contiene todos los datos (de prueba) para rellenar la bd

Una vez que localizamos ejecutamos este comando desde la terminal de:
````
 mysql -u [USUARIO_DE_BD] -h [IP_BD] -p [NOMBRE_BD] < ./db/*
````
Al hacer el comando anterior , despues pedirá la [CONTRASEÑA_BD]

"*" hace referencia al script correspondiente, primero debemos cargar la estructura de la bd, y se se quiere despues se harán los datos (muy recomendable)

## Hora de desplegar
Nos dirigiremos a la powershell (si ya tenemos el proyecto enlazado), realizaremos un pull del repositorio (si no se han realizado los cambios anteriores hay que realizarlos)

Y ya solo queda realizar el comando:
````
gcloud app deploy
````
Y si todo está en su sitio y no ha habido ningún problema, ¡enhorabuena ya tienes tu aplicacion desplegada!